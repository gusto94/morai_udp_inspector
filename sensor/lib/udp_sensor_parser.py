import socket
import threading
import time
import struct
import os
class udp_sensor_parser :
    def __init__(self,ip,port,data_type):
        self.data_type=data_type
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        recv_address = (ip,port)
        self.sock.bind(recv_address)
        self.data_size=65535 
        self.parsed_data=[]
        thread = threading.Thread(target=self.recv_udp_data)
        thread.daemon = True 
        thread.start() 

    

    def recv_udp_data(self):
        while True :
            
            raw_data, sender = self.sock.recvfrom(self.data_size)
            self.data_parsing(raw_data)




    def data_parsing(self,raw_data) :
        if self.data_type=='gps' :
            string_data=raw_data.decode()
            list_data=string_data.split(",")

            if list_data[0] == '$GPGGA' :
                print(list_data)
                print(' Latitude:{0}  Longitude {1} Altitude {2}'.format(list_data[2],list_data[4],list_data[9]))


        if self.data_type=='imu' :
            header=raw_data[0:9].decode()
            
            if header == '#IMUData$' :
                data_length=struct.unpack('i',raw_data[9:13])
                imu_data=struct.unpack('10d',raw_data[25:105])
                # round_data=[]
                # for temp in imu_data :
                #     round_data.append(round_data(temp,2))
                print('------------------------------------------------------')
                print(' ori_w:{0}  ori_x {1}  ori_y {2}  ori_z {3}'.format(round(imu_data[0],2),round(imu_data[1],2),round(imu_data[2],2),round(imu_data[3],2)))
                print(' ang_vel_x :{0}  ang_vel_y : {1}  ang_vel_z : {2} '.format(round(imu_data[4],2),round(imu_data[5],2),round(imu_data[6],2)))
                print(' lin_acc_x :{0}  lin_acc_y : {1}  lin_acc_z : {2} '.format(round(imu_data[7],2),round(imu_data[8],2),round(imu_data[9],2)))

        
    def __del__(self):
        self.sock.close()
        print('del')



