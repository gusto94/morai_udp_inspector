import socket
import cv2
import numpy as np
import time

params_cam_0 = {
    "SOCKET_TYPE": 'JPG',
    "WIDTH": 640, # image width
    "HEIGHT": 480, # image height
    "FOV": 90, # Field of view
    "localIP": "127.0.0.1",
    "localPort": 9092,
    "Block_SIZE": int(65000),
    "UnitBlock_HEIGHT": int(30),
    "X": 1.7, # meter
    "Y": 0,
    "Z": 1.2,
    "YAW": 0, # deg
    "PITCH": -5,
    "ROLL": 0
}

UDP_cam_0 = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
UDP_cam_0.bind((params_cam_0["localIP"], params_cam_0["localPort"]))


def get_img(UDPSocket_cam, params_cam):
    '''
    receive a camera image
    \n UDPSocket_cam : UDP server socket
    \n params_cam : parameters from cameras 
    '''

    if params_cam["SOCKET_TYPE"] == 'JPG':

        # JPG/UDP type

        UnitBlockSize_cam = params_cam["Block_SIZE"]
        max_len = np.floor(params_cam["WIDTH"]*params_cam["HEIGHT"]/UnitBlockSize_cam/4)-1
        
        TotalBuffer = []
        num_block = 0

        while True:

            bytesAddressPair = UDPSocket_cam.recvfrom(UnitBlockSize_cam)
            UnitBlock = bytesAddressPair[0]
            
            UnitIdx = np.frombuffer(UnitBlock[3:7], dtype = "int")[0]
            UnitSize = np.frombuffer(UnitBlock[7:11], dtype = "int")[0]
            UnitTail = UnitBlock[-2:]
            # UnitBody = np.frombuffer(UnitBlock[11:(11 + UnitSize)], dtype = "uint8")
            UnitBody = UnitBlock[11:(11 + UnitSize)]

            if num_block == UnitIdx:
                TotalBuffer.append(UnitBody)
                num_block += 1
            else:
                TotalBuffer = []
                num_block = 0

            if UnitTail==b'EI' and len(TotalBuffer)>max_len:

                TotalIMG = cv2.imdecode(np.hstack([np.frombuffer(blk, dtype = "uint8") for blk in TotalBuffer]), 1)

                break

    else:

        # RGB/UDP type

        TotalIMG = np.zeros((params_cam["HEIGHT"], params_cam["WIDTH"], 3), dtype = "uint8")
        img_head = np.zeros(int(params_cam["HEIGHT"]/params_cam["UnitBlock_HEIGHT"]),)
        UnitBlockSize_cam = int(params_cam["WIDTH"]*params_cam["UnitBlock_HEIGHT"]*3+8)

        while True:

            bytesAddressPair = UDPSocket_cam.recvfrom(UnitBlockSize_cam)
            UnitBlock = bytesAddressPair[0]

            UnitBlock_array = np.frombuffer(UnitBlock, dtype = "uint8")
            
            UnitHead = int(UnitBlock_array[0])
            UnitBody = UnitBlock_array[4:UnitBlockSize_cam-4].reshape(-1, params_cam["WIDTH"], 3)
            
            TotalIMG[UnitHead*params_cam["UnitBlock_HEIGHT"]:(UnitHead+1)*params_cam["UnitBlock_HEIGHT"],:,:] = UnitBody
            img_head[UnitHead] = 1
            
            if np.mean(img_head)>0.99:

                break

    return TotalIMG


def main():

    t_sub_udp = []

    t_loop_start = time.time()

    for i in range(1000):
    
        if time.time() - t_loop_start > 60:
            break

        # print(i)

        # measurement
        t_s = time.time()
        img_cam_0 = get_img(UDP_cam_0, params_cam_0)
        t_cost = time.time()-t_s
        t_sub_udp.append(time.time()-t_s)
        
        cv2.putText(img_cam_0,'{:.2f} Hz'.format(1/t_cost), (450,50), cv2.FONT_HERSHEY_SIMPLEX, 1,(0,255,0), 2, 0)
        cv2.imshow('Result', img_cam_0)
        cv2.waitKey(1)

    UDP_cam_0.close()
    print(t_sub_udp)
    print(np.mean(t_sub_udp))

    
if __name__ == '__main__':

    main()