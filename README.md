# UDP 테이터 확인 프로그램


## 목록  
### Sensor

- GPS
- Camera
- imu 


### Network(미완성)

- ctrl_cmd
- obj_info
- vehicle_status


----
## 설치방법

1. python을 설치(3.x.x 버전) [링크](https://www.python.org/downloads/)

        콘솔창에 python이라고 입력했을 때 다음과 같이 되면 설치된 것임.

    ![이미지](doc/1.png)


2. 파이썬 설치 후 코드를 다운로드 받은 후 압축을 푼다.
    

   ![이미지](doc/2.png)

        압축을 풀고 sensor 폴더에서 camera, gps, imu 파이썬 스크립트를 확인한다.
        

   ![이미지](doc/3.png)




## 실행방법

### GPS, IMU

gps,imu는 코드에 적힌 port와 시뮬레이터 센서 세팅의 dst 포트가 맞게 코드를 수정해주고 실행하면된다. 

![이미지](doc/4.png)
> 파이썬 스크립트 모습

![이미지](doc/5.png)

        GPS는 위도 경도 값이 콘솔창에 출력됨, IMU는 쿼터니언(x,y,z,w), 각속도(x,y,z) 각가속도(x,y,z)가 콘솔창에 출력됨
 

---


### Camera

camera도 코드에 적힌 localport와 시뮬레이터 센서 세팅의 dst 포트가 맞게 코드를 수정해주고 실행하면 된다.

![이미지](doc/6.png)
> 파이썬 스크립트 모습

![이미지](doc/7.png)

        카메라의 경우 아래와 같은 창에서 카메라 이미지가 출력됨, Ground Truth가 none 일 떄만 가능함.

![이미지](doc/8.png)
